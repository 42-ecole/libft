NAME = libft.a
CC = gcc -g
FLAGS = -Wall -Wextra -Werror
INCLUDES := -I . \
		-I $(CURDIR)/gnl/. \
		-I $(CURDIR)/enumerable/. \
		-I $(CURDIR)/enumerable/sequences/list/. \
		-I $(CURDIR)/enumerable/sequences/linked_list/. \
		-I $(CURDIR)/enumerable/collections/stack/. \
		-I ./memory/.
SRCS = memory/ft_memalloc.c \
		memory/ft_memdel.c \
		memory/ft_memset.c \
		memory/ft_memcpy.c \
		memory/ft_memccpy.c \
		memory/ft_memmove.c \
		memory/ft_memchr.c \
		memory/ft_memcmp.c \
		memory/ft_bzero.c \
		memory/ft_replace.c \
		memory/print_memory.c \
		string/ft_strstolst.c \
		string/ft_lsttostr.c \
		string/ft_strlen.c \
		string/ft_strdup.c \
		string/ft_strisdupl.c \
		string/ft_strdupbfrn.c \
		string/ft_strcpy.c \
		string/ft_strncpy.c \
		string/ft_strcat.c \
		string/ft_strncat.c \
		string/ft_strlcat.c \
		string/ft_strchr.c \
		string/ft_strrchr.c \
		string/ft_strcmp.c \
		string/ft_strncmp.c \
		string/ft_strstr.c \
		string/ft_strstri.c \
		string/ft_strnstr.c \
		string/ft_strnew.c \
		string/ft_strdel.c \
		string/ft_strdel_chars.c \
		string/ft_strclr.c \
		string/ft_striter.c \
		string/ft_striteri.c \
		string/ft_strmap.c \
		string/ft_strmapi.c \
		string/ft_strequ.c \
		string/ft_strnequ.c \
		string/ft_strsub.c \
		string/ft_strjoin.c \
		string/ft_strjoinfree.c \
		string/ft_joinargs.c \
		string/ft_freeargs.c \
		string/ft_strtrim.c \
		string/ft_strsplit.c \
		string/ft_strsplittodouble.c \
		string/ft_strint.c \
		string/ft_itoa.c \
		string/ft_utoa_base.c \
		ft_atoi.c \
		ft_atof.c \
		ft_rm_char.c \
		ft_is_hex.c \
		ft_atol.c \
		ft_isalpha.c \
		ft_isdigit.c \
		ft_isalnum.c \
		ft_isnum.c \
		ft_isascii.c \
		ft_isprint.c \
		ft_toupper.c \
		ft_tolower.c \
		ft_putchar.c \
		ft_putstr.c \
		ft_putendl.c \
		ft_putnbr.c \
		ft_putchar_fd.c \
		ft_putstr_fd.c \
		ft_putendl_fd.c \
		ft_putnbr_fd.c \
		ft_putstrtab.c \
		enumerable/ft_foreach.c \
		enumerable/object.c \
		enumerable/sequences/list/lst_new.c \
		enumerable/sequences/list/lst_enum.c \
		enumerable/sequences/list/lst_manage.c \
		enumerable/sequences/list/lst_sort.c \
		enumerable/sequences/linked_list/llst_new.c \
		enumerable/sequences/linked_list/llst_enum.c \
		enumerable/sequences/linked_list/llst_manage.c \
		enumerable/collections/stack/stack_new.c \
		enumerable/collections/stack/stack_manage.c \
		enumerable/collections/hash_table/hasht_new.c \
		enumerable/collections/hash_table/hasht_manage.c \
		ft_swap.c \
		ft_atoi_base.c \
		manage_bits.c \
		ft_set_in_range.c \
		ft_is_in_range.c \
		ft_curdir.c \
		gnl/get_next_line.c \
		printf/ft_printf.c \
		printf/srcs/ascii_manager.c \
		printf/srcs/f_output.c \
		printf/srcs/parser.c \
		printf/srcs/funcs.c \
		printf/srcs/manages/mng_address.c \
		printf/srcs/manages/mng_bits.c \
		printf/srcs/manages/mng_char.c \
		printf/srcs/manages/mng_conversion.c \
		printf/srcs/manages/mng_decimal.c \
		printf/srcs/manages/mng_double.c \
		printf/srcs/manages/mng_hex.c \
		printf/srcs/manages/mng_octal.c \
		printf/srcs/manages/mng_string.c \
		printf/srcs/manages/mng_unsigned.c \

OBJ = $(SRCS:.c=.o)

#-----------STYLES-----------------#
NONE = \033[0m
INVERT := \033[7m
GREEN := \033[32m
RED := \033[31m
SUCCESS := [$(GREEN)✓$(NONE)]
COMPILATIONOK := $(INVERT)$(GREEN)Compiled ✓$(NONE)$(INVERT):$(NAME) has been successfully compiled.$(NONE)
LIBDELETED := $(INVERT)$(RED)Removed ✓$(NONE)$(INVERT):$(NAME) has been successfully removed.$(NONE)
#----------------------------------#
DEL := rm -rf

all: $(NAME)

$(OBJ): %.o: %.c
	@echo -n $(NAME):' $@: '
	@$(CC) -c $(FLAGS) $(INCLUDES) $< -o $@
	@echo "$(SUCCESS)"

$(NAME): $(OBJ)
	@ar rc $(NAME) $(OBJ)
	@ranlib $(NAME)
	@echo "$(COMPILATIONOK)"

clean:
	@$(DEL) $(OBJ)

fclean: clean
	@$(DEL) $(NAME)
	@echo "$(LIBDELETED)"

norm:
	@norminette $(SRCS)

re: fclean all
