/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstolst.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/12 20:44:12 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 20:45:00 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_strstolst(char **strs, size_t count)
{
	t_list		*lst;
	size_t		i;

	i = -1;
	lst = new_list(count, 256); 
	while (++i < count)
		lst->add(lst, strs[i]);
	return lst;
}
