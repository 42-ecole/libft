/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freeargs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 19:22:07 by mdooley           #+#    #+#             */
/*   Updated: 2020/10/28 16:40:44 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_freeargs(int count, ...)
{
	va_list args;
	void	*vp;
	int		i;

	i = 0;
	va_start(args, count);
	while (i < count)
	{
		vp = va_arg(args, void *);
		free(vp);
		i++;
	}
	va_end(args);
}
