/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_joinargs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 15:26:48 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 17:57:40 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		count_args(va_list ap, int count)
{
	int		i;
	int		len;

	i = 0;
	len = 1;
	while (i++ < count)
		len += ft_strlen(va_arg(ap, char*));
	va_end(ap);
	return (len);
}

char	*ft_joinargs(int count, ...)
{
	va_list	ap;
	int		i;
	int		null_pos;
	char	*merged;
	char	*s;

	i = 0;
	null_pos = 0;
	va_start(ap, count);
	merged = calloc(sizeof(char), count_args(ap, count));
	va_start(ap, count);
	while (i++ < count)
	{
		s = va_arg(ap, char*);
		ft_strcpy(merged + null_pos, s);
		null_pos += ft_strlen(s);
		free(s);
	}
	va_end(ap);
	return (merged);
}
