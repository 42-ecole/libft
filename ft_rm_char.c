/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rm_char.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <mdooley@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/11 19:04:26 by mdooley           #+#    #+#             */
/*   Updated: 2020/10/12 19:35:11 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int				ft_rm_char(char *dest, char c)
{
	int		removed;
	char	*tmp;
	size_t	len;

	removed = 0;
	while (*dest)
	{
		tmp = ft_strchr(dest, c);
		if (NULL == tmp)
			break ;
		len = ft_strlen(tmp + 1);
		ft_memmove(tmp, tmp + 1, len);
		tmp[len] = 0;
		++removed;
		dest = tmp;
	}
	return (removed);
}
