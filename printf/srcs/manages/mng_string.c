/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mng_string.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:07:41 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/04 14:11:46 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

static char		*get_simple_string(t_format *f)
{
	char	*str;
	va_list	copy;

	if (f->keys & DOLL)
	{
		va_copy(copy, f->lst);
		while (--f->argc > 0)
			va_arg(copy, void*);
	}
	str = va_arg((f->keys & DOLL) ? copy : f->lst, char*);
	if (!str)
		str = "(null)";
	return (str);
}

static wchar_t	*get_wide_string(t_format *f)
{
	wchar_t	*wstr;
	va_list	copy;

	if (f->keys & DOLL)
	{
		va_copy(copy, f->lst);
		while (--f->argc > 0)
			va_arg(copy, void*);
	}
	wstr = va_arg((f->keys & DOLL) ? copy : f->lst, wchar_t*);
	if (!wstr)
		wstr = L"(null)";
	return (wstr);
}

static int		get_length(t_format *f, wchar_t *wstr)
{
	int len;

	len = 0;
	while (*wstr && f->prec > 0)
	{
		if (*wstr < 0X80 && --f->prec >= 0)
			len++;
		else if (*wstr < 0X800 && (f->prec -= 2) >= 0)
			len += 2;
		else if (*wstr < 0X10000 && (f->prec -= 3) >= 0)
			len += 3;
		else if (*wstr < 0X110000 && (f->prec -= 4) >= 0)
			len += 4;
	}
	return (len);
}

static void		manage_wide_string(t_format *f)
{
	wchar_t	*wstr;
	int		len;

	wstr = get_wide_string(f);
	len = (f->keys & PREC) ? get_length(f, wstr) : ft_wstr_len(wstr);
	f->w -= len;
	if (f->w > 0 && !(f->keys & ALIGN))
		add_values(f, (f->keys & ZERO) ? 48 : 32, f->w);
	while (len > 0)
	{
		if (*wstr < 0X80)
			len--;
		else if (*wstr < 0X800)
			len -= 2;
		else if (*wstr < 0X10000)
			len -= 3;
		else if (*wstr < 0X110000)
			len -= 4;
		add_wchar(f, *(wstr++));
	}
	if (f->w > 0 && f->keys & ALIGN)
		add_values(f, 32, f->w);
}

void			mng_string(t_format *f)
{
	char	*str;
	int		len;

	if (*(f->pos) == 'S' || f->keys & LONG)
	{
		manage_wide_string(f);
		return ;
	}
	str = get_simple_string(f);
	len = ft_strlen2(str);
	f->prec = (f->keys & PREC && f->prec < len) ? f->prec : len;
	len = f->w - f->prec;
	if (len > 0 && !(f->keys & ALIGN))
		add_values(f, (f->keys & ZERO) ? 48 : 32, len);
	while (f->prec--)
		add_char(f, *(str++));
	if (len > 0 && f->keys & ALIGN)
		add_values(f, 32, len);
}
