/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mng_char.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:06:41 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/04 14:14:24 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

static void	get_wchar(t_format *f, wchar_t *c)
{
	va_list	copy;

	if (f->keys & DOLL)
	{
		va_copy(copy, f->lst);
		while (--f->argc > 0)
			va_arg(copy, void*);
	}
	if (*(f->pos) == 'C' || *(f->pos) == 'c')
		*c = va_arg((f->keys & DOLL) ? copy : f->lst, wchar_t);
	else
		*c = *(f->pos);
	if (*(f->pos) == 'c' && !(f->keys & LONG))
		*c = (char)*c;
}

void		mng_char(t_format *f)
{
	wchar_t c;

	get_wchar(f, &c);
	f->w -= ft_wchar_len(c);
	if (f->w > 0 && !(f->keys & ALIGN))
		add_values(f, (f->keys & ZERO) ? 48 : 32, f->w);
	add_wchar(f, c);
	if (f->w > 0 && f->keys & ALIGN)
		add_values(f, 32, f->w);
}
