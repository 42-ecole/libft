/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mng_address.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:06:31 by rfunk             #+#    #+#             */
/*   Updated: 2021/01/13 21:35:46 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	get_size(t_format *f, long addr, size_t *size)
{
	*size = (addr == 0) ? 1 : 0;
	while (addr > 0)
	{
		addr /= 16;
		*size += 1;
	}
	if (f->keys & PREC)
		*size = (f->prec > (int)*size) ? f->prec : *size;
	if (f->keys & WIDTH && f->keys & ZERO)
		*size = (f->w - 2 > (int)*size) ? f->w - 2 : *size;
}

static void	add_adress(t_format *f, long addr, size_t size)
{
	if (f->keys & PREC && f->prec == 0)
		return ;
	if (size > 0)
	{
		add_adress(f, addr / 16, --size);
		add_char(f, (addr % 16 > 9) ? addr % 16 + 87 : addr % 16 + 48);
	}
}

static void	get_address(t_format *f, long *addr)
{
	va_list copy;

	if (f->keys & DOLL)
	{
		va_copy(copy, f->lst);
		while (--f->argc > 0)
			va_arg(copy, void*);
	}
	*addr = va_arg((f->keys & DOLL) ? copy : f->lst, long);
}

void		mng_address(t_format *f)
{
	long	addr;
	size_t	size;

	get_address(f, &addr);
	get_size(f, addr, &size);
	if (!(f->keys & ALIGN) && !(f->keys & ZERO))
		add_values(f, 32, f->w - size - 2);
	add_char(f, '0');
	add_char(f, 'x');
	add_adress(f, addr, size);
	if (f->keys & ALIGN)
		add_values(f, 32, f->w - size - 2);
}
