/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mng_decimal.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:06:59 by rfunk             #+#    #+#             */
/*   Updated: 2021/01/13 21:36:08 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	get_decimal(t_format *f, intmax_t *decimal)
{
	va_list copy;

	if (f->keys & DOLL)
	{
		va_copy(copy, f->lst);
		while (--f->argc > 0)
			va_arg(copy, void*);
	}
	if (f->keys & SHORT)
		*decimal = (short)va_arg((f->keys & DOLL) ? copy : f->lst, int);
	else if (f->keys & SHORT1)
		*decimal = (char)va_arg((f->keys & DOLL) ? copy : f->lst, int);
	else if (f->keys & LONG || *(f->pos) == 'D')
		*decimal = va_arg((f->keys & DOLL) ? copy : f->lst, long int);
	else if (f->keys & LONG1)
		*decimal = va_arg((f->keys & DOLL) ? copy : f->lst, long long int);
	else if (f->keys & INTMAX)
		*decimal = va_arg((f->keys & DOLL) ? copy : f->lst, intmax_t);
	else if (f->keys & SIZE_T)
		*decimal = va_arg((f->keys & DOLL) ? copy : f->lst, size_t);
	else
		*decimal = va_arg((f->keys & DOLL) ? copy : f->lst, int);
}

static void	get_size(t_format *f, intmax_t decimal, int *size)
{
	if (decimal < 0)
		f->keys |= MINUS;
	*size = (decimal == 0) ? 1 : 0;
	while (decimal != 0)
	{
		decimal /= 10;
		*size += 1;
	}
	if (f->keys & PREC)
		*size = (f->prec > *size) ? f->prec : *size;
	if (f->keys & PLUS || f->keys & SPACE || f->keys & MINUS)
		*size += 1;
	if (f->keys & WIDTH && f->keys & ZERO)
		*size = (f->w > *size) ? f->w : *size;
}

static void	print_decimal(t_format *f, intmax_t decimal, int size)
{
	if (size > 0)
	{
		print_decimal(f, decimal / 10, --size);
		if (f->keys & PREC && f->prec == 0 && decimal == 0)
		{
			if (f->w > 0)
				add_char(f, 32);
			return ;
		}
		if (decimal < 0)
			add_char(f, -(decimal % 10 - 48));
		else
			add_char(f, decimal % 10 + 48);
	}
}

void		mng_decimal(t_format *f)
{
	bool		sign;
	int			size;
	intmax_t	decimal;

	get_decimal(f, &decimal);
	get_size(f, decimal, &size);
	sign = (f->keys & PLUS || f->keys & SPACE || f->keys & MINUS) ? 1 : 0;
	if (!(f->keys & ALIGN) && !(f->keys & ZERO))
		add_values(f, 32, f->w - size);
	if (sign)
	{
		add_sign(f);
		print_decimal(f, decimal, --size);
	}
	else
		print_decimal(f, decimal, size);
	if (f->keys & ALIGN)
		add_values(f, 32, (sign) ? f->w - size - 1 : f->w - size);
}
