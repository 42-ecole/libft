/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mng_bits.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:06:36 by rfunk             #+#    #+#             */
/*   Updated: 2021/01/13 21:35:51 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	print_bits(t_format *f, intmax_t bits)
{
	intmax_t	bit;

	bit = 1;
	add_char(f, 98);
	if (bits < 0)
		bits = bits * -1 + 1;
	while (bit < bits)
		bit *= 2;
	if (bit > bits)
		bit /= 2;
	while (bit != 0)
	{
		if (bits / bit > 0)
		{
			add_char(f, bits / bit + 48);
			bits %= bit;
			bit /= 2;
		}
		else
		{
			add_char(f, 48);
			bit /= 2;
		}
	}
}

static void	get_value(t_format *f, intmax_t *bits)
{
	va_list			copy;

	if (f->keys & DOLL)
	{
		va_copy(copy, f->lst);
		while (--f->argc > 0)
			va_arg(copy, void*);
	}
	*bits = va_arg((f->keys & DOLL) ? copy : f->lst, intmax_t);
}

void		mng_bits(t_format *f)
{
	intmax_t bits;

	get_value(f, &bits);
	print_bits(f, bits);
}
