/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mng_conversion.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:06:47 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/10 13:17:04 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	mng_conversion(t_format *f)
{
	printf_parse_flags(f);
	if (*(f->pos) == 'p')
		mng_address(f);
	else if (*(f->pos) == 'd' || *(f->pos) == 'D' || *(f->pos) == 'i')
		mng_decimal(f);
	else if (*(f->pos) == 'S' || *(f->pos) == 's')
		mng_string(f);
	else if (*(f->pos) == 'u' || *(f->pos) == 'U')
		mng_unsigned(f);
	else if (*(f->pos) == 'x' || *(f->pos) == 'X')
		mng_hex(f);
	else if (*(f->pos) == 'o' || *(f->pos) == 'O')
		mng_octal(f);
	else if (*(f->pos) == 'f' || *(f->pos) == 'F')
		mng_double(f);
	else if (*(f->pos) == 'b')
		mng_bits(f);
	else
		mng_char(f);
}
