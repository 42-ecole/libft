/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   f_output.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/12 12:06:05 by rfunk             #+#    #+#             */
/*   Updated: 2021/01/13 21:37:19 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void			add_char(t_format *f, char c)
{
	if (f->len == (int)f->buf_size)
		if (!double_output(f))
			write(1, "Allocation error!", 17);
	f->output[f->len++] = c;
}

void			add_string(t_format *f, char *str)
{
	while (*str)
	{
		add_char(f, *str);
		str++;
	}
}

void			add_wchar(t_format *f, wchar_t c)
{
	if (c < 0x80)
		add_char(f, c);
	else if (c < 0x800)
	{
		add_char(f, WCHAR21);
		add_char(f, WCHAR22);
	}
	else if (c < 0x10000)
	{
		add_char(f, WCHAR31);
		add_char(f, WCHAR32);
		add_char(f, WCHAR33);
	}
	else if (c < 0x110000)
	{
		add_char(f, WCHAR41);
		add_char(f, WCHAR42);
		add_char(f, WCHAR43);
		add_char(f, WCHAR44);
	}
}

void			add_sign(t_format *f)
{
	if (f->keys & MINUS)
		add_char(f, 45);
	else if (f->keys & PLUS)
		add_char(f, 43);
	else
		add_char(f, 32);
}

bool			double_output(t_format *f)
{
	char	*tmp;
	size_t	i;

	i = -1;
	f->buf_size += 100;
	if (!(tmp = ft_strnew(f->buf_size)))
	{
		free(f->output);
		exit(-1);
	}
	while (f->output[++i])
		tmp[i] = f->output[i];
	free(f->output);
	f->output = tmp;
	return (1);
}
