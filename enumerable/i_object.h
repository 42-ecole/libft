/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_object.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/10 16:52:04 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/04 16:58:55 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef I_OBJECT_H
# define I_OBJECT_H

# include "stdlib.h"
# include "../memory/memory.h"

typedef struct		s_object
{
	void					*content;
	size_t					content_size;
	struct s_object			*next;
}					t_object;

t_object		*new_object(void const *content, size_t content_size);

#endif