/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   object.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/26 18:24:40 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/04 16:58:55 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "i_object.h"

t_object		*new_object(void const *content, size_t content_size)
{
	t_object *chain;

	chain = NULL;
	if (!(chain = ft_memalloc(sizeof(t_object))))
		return (NULL);
	if (!content)
	{
		chain->content = NULL;
		chain->content_size = 0;
	}
	else
	{
		if (!(chain->content = ft_memalloc(content_size)))
		{
			free(chain);
			return (NULL);
		}
		chain->content = ft_memcpy(chain->content, content, content_size);
		chain->content_size = content_size;
	}
	chain->next = NULL;
	return (chain);
}
