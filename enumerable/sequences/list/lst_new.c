/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_new.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/18 17:05:05 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 17:39:24 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"
#include "list_p.h"
#include <stdio.h>

/*
	Creates list with excepted capacity with size for each object.
	\param capacity maximum expected number of items. If the actual quantity is more than the capacity,
	the list will request memory for the current capacity and double it.
	\param content_size size of stored data. Use everywhere one type. For another tasks use List.


	\param add() can be overloaded with your implementation.
	\param remove() can be overloaded.
	\param is_sorted() can be overloaded.
	\param enumerable can be overloaded.
*/
t_list		*new_list(size_t capacity, size_t content_size)
{
	t_list 		*lst;

	if (!capacity || !content_size)
		return (NULL);
	lst = ft_memalloc(sizeof(t_list) + content_size * capacity);
	lst->capacity = capacity;
	lst->content_size = content_size;
	lst->index = 0;
	lst->count = 0;
	lst->objs = ((unsigned char *)lst + sizeof(t_list));

	// initialize pointers to functions
	lst->enumerable.move_next = lst_movenext;
	lst->enumerable.has_next = lst_hasnext;
	lst->enumerable.current = lst_current;
	lst->enumerable.reset = lst_reset;
	lst->enumerable.get_content_size = lst_get_content_size;
	lst->enumerable.get_count = lst_get_count;
	lst->index_of = lst_index_of;
	lst->is_sorted = lst_is_descorder;
	lst->reverse = lst_reverse;
	lst->shift = lst_shift;
	lst->add = lst_add;
	lst->add_f = lst_add_free;
	lst->add_size = lst_add_size;
	lst->add_range = lst_addrng;
	lst->remove = lst_remove;
	return (lst);
}
