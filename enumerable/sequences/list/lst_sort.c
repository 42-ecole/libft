/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_sort.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/08 16:51:06 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/11 16:09:34 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"
#include "list_p.h"
#include <stdio.h>

void	ft_swap(int *x, int *y)
{
	int z;

	z = *x;
	*x = *y;
	*y = z;
}

bool	comparator_int(void *el, void *el2)
{
	int		*num;
	int 	*num2;

	num = (int *)el;
	num2 = (int *)el2;
	if (*num > *num2)
		return (true);
	return (false);
}

void	*calc_pivot(t_list *lst)
{
	int		high;
	int		mid_index;
	
	high = *CAST_TYPE(int, lst->index_of(lst, lst->count));
	mid_index = lst->count / 2;
	if (lst->index_of(lst, mid_index) < lst->index_of(lst, 0))
		ft_swap(lst->index_of(lst, 0), lst->index_of(lst, mid_index));
	if (lst->index_of(lst, high) < lst->index_of(lst, 0))
		ft_swap(lst->index_of(lst, 0), lst->index_of(lst, high));
	if (lst->index_of(lst, mid_index) < lst->index_of(lst, high))
		ft_swap(lst->index_of(lst, high), lst->index_of(lst, mid_index));
	return (lst->index_of(lst, high));
}

int		partition_int(t_list *lst, int low, int high,
			bool (*comparator)(void *, void*))
{
	int		i;
	int		j;

	i = low;
	j = high + 1;
	while(true)
	{
		while (comparator(lst->index_of(lst, low),
				lst->index_of(lst, ++i)))
			if ( i == high )
				break;
		while (comparator(lst->index_of(lst, --j),
				lst->index_of(lst, low)))
			if ( j == low )
				break;
		if (i >= j)
			break;
		ft_swap(lst->index_of(lst, i), lst->index_of(lst, j));
	}
	ft_swap(lst->index_of(lst, low), lst->index_of(lst, j));
	return (j);
}

void	lst_sort(void *sequence, int low, int high,
			bool (*comparator)(void *, void*))
{
	t_list 	*lst;
	int		i;

	lst = (t_list *)sequence;
	if (high <= low)
		return ;
	i = partition_int(lst, low, high, comparator);
	lst_sort(lst, low, i - 1, comparator);
	lst_sort(lst, i + 1, high, comparator);
}