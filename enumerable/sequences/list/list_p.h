/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_p.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/04 14:16:37 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/12 17:39:33 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_P_H
# define LIST_P_H

# include <stdbool.h>
# include "list.h"

bool	lst_movenext(void *enumerable);
bool	lst_hasnext(void *enumerable);
void	lst_reset(void *enumerable);
void	*lst_current(void *enumerable);
size_t	lst_get_content_size(void *sequence);
size_t	lst_get_count(void *sequence);
void    lst_add(struct s_list *lst, void *data);
void	lst_add_free(struct s_list *lst, void *data);
void	lst_add_size(struct s_list *lst, void *data, size_t content_size);
void	lst_addrng(t_list *self, t_list *lst);
void	*lst_remove(t_list *lst, int index);
void	*lst_index_of(struct s_list *lst, int index);
void	lst_reverse(t_list *lst, int start, int end);
void	lst_shift(t_list *lst, int offset);
#endif
