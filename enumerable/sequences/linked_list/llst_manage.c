/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llst_manage.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/30 17:18:03 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/04 14:17:18 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "linked_list.h"
#include "linked_list_p.h"

// void		llst_addobj(struct s_l_list *lst, t_object *obj)
// {
// 	t_object	*new;

// 	lst->count++;
// 	new = (t_object *)obj;
// 	if (lst->ptr)
// 	{
// 		new->next = lst->ptr;
// 		lst->ptr = new;
// 		*(lst->head) = lst->ptr;
// 	}
// 	else
// 	{
// 		*(lst->head) = new;
// 		lst->enumerable.reset(lst);
// 	}
// }

void		llst_addobj(struct s_llist *lst, t_object *obj)
{
	t_object	*new;

	lst->count++;
	new = (t_object *)obj;
	if (lst->ptr)
	{
		lst->ptr->next = new;
		lst->enumerable.move_next(lst);
	}
	else
	{
		*(lst->head) = new;
		lst->enumerable.reset(lst);
	}
}

void		llst_add(void *dst_lst, void *data)
{
	t_object	*new;
	t_llist		*lst;

	lst = (t_llist *)dst_lst;
	if (!lst)
		return;
	new = new_object(data, lst->content_size);
	llst_addobj(lst, new);
}


// void		linked_list_add(t_llist *lst, t_object *new)
// {
// 	t_object	*end;

// 	if (lst && new)
// 	{
// 		end = lst->head;
// 		while (end && end->next)
// 			end = end->next;
// 		if (end)
// 			end->next = new;
// 		else
// 			lst->head = new;
// 		lst->count++;
// 	}
// }

// void		ft_lstdel(t_llist **alst, void (*selector)(void *, size_t))
// {
// 	t_object	*temp;

// 	if (!selector || !alst)
// 		return ;
// 	while (*alst)
// 	{
// 		temp = (*alst)->next;
// 		selector(((*alst)->content), (*alst)->content_size);
// 		free(*alst);
// 		*alst = temp;
// 	}
// 	*alst = NULL;
// }
