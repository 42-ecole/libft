/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llst_new.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/26 18:26:20 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/04 20:33:01 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "linked_list.h"
#include "linked_list_p.h"

t_llist		*new_llist(size_t content_size)
{
	t_llist 	*lst;

	lst = ft_memalloc(sizeof(t_llist));
	lst->head = ft_memalloc(sizeof(t_object *));
	lst->count = 0;
	lst->content_size = content_size;
	lst->enumerable.move_next = llst_movenext;
	lst->enumerable.has_next = llst_hasnext;
	lst->enumerable.current = llst_current;
	lst->enumerable.reset = llst_reset;
	lst->enumerable.get_content_size = llst_get_content_size;
	lst->enumerable.get_count = llst_get_count;
	lst->add = llst_add;
	lst->add_obj = llst_addobj;
	return (lst);
}

// t_llist		*new_llist(void *dst, size_t content_size)
// {
// 	t_llist 	**lst;

// 	lst = (t_llist **)dst;
// 	if (!*lst)
// 		*lst = ft_memalloc(sizeof(t_llist));
// 	(*lst)->head = NULL;
// 	(*lst)->count = 0;
// 	(*lst)->content_size = content_size;
// 	(*lst)->enumerable.move_next = llst_movenext;
// 	(*lst)->enumerable.has_next = llst_hasnext;
// 	(*lst)->enumerable.current = llst_current;
// 	(*lst)->enumerable.reset = llst_reset;
// 	(*lst)->add = llst_add;
// 	return (*lst);
// }
