// /* ************************************************************************** */
// /*                                                                            */
// /*                                                        :::      ::::::::   */
// /*   ft_lstselect.c                                     :+:      :+:    :+:   */
// /*                                                    +:+ +:+         +:+     */
// /*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
// /*                                                +#+#+#+#+#+   +#+           */
// /*   Created: 2020/10/09 18:33:15 by Diana             #+#    #+#             */
// /*   Updated: 2020/12/04 16:58:55 by rfunk            ###   ########.fr       */
// /*                                                                            */
// /* ************************************************************************** */

// #include "linked_list.h"

// t_object		*element_at(t_llist *source, size_t index)
// {
// 	t_object *ptr;

// 	ptr = *(source->head);
// 	while (ptr)
// 	{
// 		if (!index--)
// 			return (ptr);
// 		ptr = ptr->next;
// 	}
// 	return (NULL);
// }

// void	*ft_lstselect(t_llist *source,
// 			bool (*selector)(t_object *, void *), void *cmp)
// {
// 	t_object	*ptr;
// 	t_object	*selected;

// 	if (!source)
// 		return (NULL);
// 	ptr = *(source->head);
// 	while (ptr)
// 	{
// 		if (selector(ptr, cmp))
// 		{
// 			void *data = ft_memalloc(ptr->content_size);
// 			ft_memcpy(data, ptr, ptr->content_size);
// 			selected = (t_object *)data;
// 			selected->next = NULL;
// 			return (data);
// 			// selected = new_object(ptr->content, ptr->content_size);
// 			// selected->next = NULL;
// 			// return (selected);
// 		}
// 		ptr = ptr->next;
// 	}
// 	return (NULL);
// }
