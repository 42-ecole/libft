#ifndef LINKED_LIST_H
# define LINKED_LIST_H

# include <stdbool.h>
# include <string.h>
# include <stdlib.h>
# include "../../../memory/memory.h"
# include "../../i_object.h"
# include "../../enumerable.h"


typedef struct	s_llist
{
	t_object				**head;
	t_object				*ptr;
	int						count;
	size_t					content_size;
	struct s_ienumerable	enumerable;
	void					(*add)(void *lst, void *);
	void					(*add_obj)(struct s_llist *lst, t_object *);
	void					(*remove)(struct s_llist *,
								void (*selector)(void *, size_t));
}				t_llist;

t_llist		*new_llist(size_t content_size);

#endif