/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_table.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/24 17:12:18 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/06 15:59:01 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HASH_TABLE_H
# define HASH_TABLE_H

#include "../../sequences/list/list.h"
#include "../../sequences/linked_list/linked_list.h"

typedef struct	s_hasht_item
{
	unsigned int	hash;
	void			*value;
}				t_hasht_item;


typedef struct	s_hash_table
{
	t_list			*seq;
	size_t			count;
	size_t			content_size;
	unsigned int	(*hash)(void *);
	void			(*add)(struct s_hash_table *, void *);
	void			(*remove)(struct s_hash_table *, void *);
	void			*(*search)(struct s_hash_table *, void *);
}				t_hash_table;

t_hash_table	*new_hash_table(size_t capacity,
				size_t content_size, unsigned int (*hash_func)(void *));

#endif