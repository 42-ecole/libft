/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hasht_manage.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/08 10:45:41 by rfunk             #+#    #+#             */
/*   Updated: 2021/05/06 15:59:01 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hash_table.h"
#include "hash_table_p.h"

void			hasht_add(t_hash_table *table, void *data)
{
	unsigned int	hash;
	t_llist		*lst;
	t_llist		*cpy;
	
	hash = table->hash(data);
	lst = CAST_TYPE(t_llist,
				table->seq->index_of(table->seq, hash));
	if (!lst->count)
	{
		cpy = new_llist(table->seq->content_size);
		ft_memcpy(lst, cpy, sizeof(t_llist));
	}
	lst->add(lst, data);
}

void			*hash_search(t_hash_table *table, void *data)
{
	unsigned int	hash;

	hash = table->hash(data);
	return (table->seq->index_of(table->seq, hash));
}

// void			hasht_remove(t_hash_table *table, void *data)
// {

// }
