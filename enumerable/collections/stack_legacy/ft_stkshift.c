/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stkshift.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/17 11:56:54 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/17 12:52:35 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"
#include <stdio.h>

//TODO rework
void    stk_shift(t_istack *s, int offset)
{
	t_object    *p;
	t_object	*n;
	int			count;

	if (offset < 0)
	{
		offset *= -1;
		offset = offset % s->count;
		offset = s->count - offset;
	}
	else if (offset == 0)
		return ;
	p = s->objs;
    while(p->next)
        p = p->next;
    p->next = s->objs;
    
    n = s->objs;;
    count = 1;
    while(count < offset)
    {
        n = n->next;
    	count++;
    }
    s->objs = n->next;
    n->next = NULL;
	s->enumerable.reset(s);
}
