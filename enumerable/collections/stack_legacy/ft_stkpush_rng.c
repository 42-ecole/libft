// /* ************************************************************************** */
// /*                                                                            */
// /*                                                        :::      ::::::::   */
// /*   ft_stkpush_rng.c                                   :+:      :+:    :+:   */
// /*                                                    +:+ +:+         +:+     */
// /*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
// /*                                                +#+#+#+#+#+   +#+           */
// /*   Created: 2018/11/26 18:22:35 by rfunk             #+#    #+#             */
// /*   Updated: 2020/10/29 15:17:50 by rfunk            ###   ########.fr       */
// /*                                                                            */
// /* ************************************************************************** */

// #include "libft.h"

// void		ft_stkpush_rng(t_stack **dest, t_stack **src)
// {
// 	t_stack		*tmp;

// 	if (dest && src)
// 	{
// 		tmp = *src;
// 		while (tmp->next)
// 			tmp = tmp->next;
// 		if (*dest)
// 		{
// 			tmp->next = *dest;
// 			*dest = *src;
// 		}
// 		else
// 			*dest = *src;
// 	}
// }
