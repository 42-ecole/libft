/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stkis_ascorder.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/17 14:36:34 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/17 15:36:34 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

bool	stk_is_ascorder(t_istack *s)
{
	t_object	*obj;
	t_object	*obj2;
	int			*num;
	int			*num2;
	int			i;

	i = 0;
	do
	{
		obj = s->enumerable.current(s);
		if (i + 1 < (int)s->count)
		{
			obj2 = obj->next;
			i++;
		}
		num = (int *)obj->content;
		num2 = (int *)obj2->content;
		if (*num > *num2)
			return (false);
	}
	while (s->enumerable.move_next(s));
	return (true);
}
