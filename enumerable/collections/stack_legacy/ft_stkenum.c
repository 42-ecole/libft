/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stkenum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/30 18:58:20 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/09 16:32:14 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

bool	stk_movenext(void *enumerable)
{
	t_istack *stack;

	stack = (t_istack *)enumerable;
	if (!(stack->ptr->next))
		return (false);
	stack->ptr = stack->ptr->next;
	return (true);
}

bool	stk_hasnext(void *enumerable)
{
	t_istack *stack;

	stack = (t_istack *)enumerable;
	if (!(stack->ptr->next))
		return (false);
	return (true);
}

void	stk_reset(void *enumerable)
{
	t_istack *stack;

	stack = (t_istack *)enumerable;
	stack->ptr = stack->objs;
}

void	*stk_current(void *enumerable)
{
	t_istack *stack;

	stack = (t_istack *)enumerable;
	return (stack->ptr);
}

void		stk_link_enumerable(struct s_ienumerable *enumerable)
{
	enumerable->current = stk_current;
	enumerable->move_next = stk_movenext;
	enumerable->has_next = stk_hasnext;
	enumerable->reset = stk_reset;
}