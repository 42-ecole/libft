/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stkreverse.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/17 11:57:08 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/17 12:01:42 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"

void    stk_reverse(t_istack *stack, int start, int end)
{
    // todo implement with gcc -Werror flag
    stack->count++;
    stack->count--;
}