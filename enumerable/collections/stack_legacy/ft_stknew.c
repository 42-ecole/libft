/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stknew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/28 16:42:40 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/26 18:24:22 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"

t_istack		*ft_newstk(size_t content_size)
{
	t_istack		*stack;

	stack = ft_memalloc(sizeof(t_istack));
	stack->objs = ft_memalloc(sizeof(t_object **));
	stk_link_enumerable(&(stack->enumerable));
	stack->count = 0;
	stack->content_size = content_size;
	stack->push = stk_push;
	stack->push_obj = stk_pushobj;
	stack->pop = stk_pop;
	stack->pop_obj = stk_popobj;
	stack->peek = stk_peek;
	stack->index_of = stk_indexof;
	stack->reverse = stk_reverse;
	stack->shift = stk_shift;
	stack->is_sorted = stk_is_ascorder;
	return (stack);
}
