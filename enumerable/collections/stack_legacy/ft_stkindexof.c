/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stkindexof.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/13 17:02:49 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/13 17:40:23 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"

void		*stk_indexof(struct s_istack *stack, int index)
{
    t_object    *tmp;
    t_object    *obj;

    tmp = stack->ptr;
    stack->enumerable.reset(stack);
    if (index >= 0)
	    while (index && stack->enumerable.move_next(stack))
            index--;
    obj = stack->ptr;
    stack->ptr = tmp;
    return (obj->content);
}