/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fstkpop.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/28 16:42:40 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/14 16:49:35 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include <stdio.h>
t_object		*fstk_popobj(t_istack *stack)
{
	t_object	*obj;
	
	obj = (stack->objs + stack->count - 1);
	stack->ptr = obj - 1;
	stack->count--;
	return (obj);
}

void		*fstk_pop(t_istack *stack)
{
	t_object	*obj;
	
	if ((obj = fstk_popobj(stack)))
		return (obj->content);
	return (NULL);
}
