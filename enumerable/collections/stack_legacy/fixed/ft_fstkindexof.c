/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fstkindexof.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/13 17:02:49 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/13 17:33:57 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"

void		*fstk_indexof(struct s_istack *stack, int index)
{
    if (index < 0)
        return (stack->objs[stack->max_size - 1].content);
    if (stack->max_size < (size_t)index + 1)
        return (stack->objs[0].content);
    return (stack->objs[stack->max_size - index - 1].content);
}
