/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fstkshift.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/16 12:58:12 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/17 12:35:13 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"

void	fstk_shift(t_istack *s, int offset)
{
	if (offset < 0)
	{
		offset *= -1;
		offset = offset % s->count;
		offset = s->count - offset;
	}
	else
		offset = offset % s->count;
	s->reverse(s, 0, s->count - 1);
	s->reverse(s, 0, offset - 1);
	s->reverse(s, offset, s->count - 1);
}
