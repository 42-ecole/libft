/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fstkpush.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/28 16:42:40 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/14 16:49:32 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stdio.h"


void		fstk_push(struct s_istack *stack, void *data)
{
	stack->count++;
	if (stack->ptr)
	{
		if (stack->enumerable.move_next(stack))
			stack->ptr->content = data;
	}
	else
	{
		stack->objs[0].content = data;
		stack->enumerable.reset(stack);
	}
}
