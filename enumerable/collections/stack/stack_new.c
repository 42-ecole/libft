/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/23 17:25:34 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/09 12:00:09 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"

// void	stack_reset(void *enumerable)
// {
// 	t_stack *stack;

// 	stack = (t_stack *)enumerable;
// 	stack->seq->index = stack->seq->count;
// }

t_stack		*new_stack(size_t capacity, size_t content_size)
{
	t_stack		*stack;

	stack = ft_memalloc(sizeof(t_stack));
	stack->seq = new_list(capacity, content_size);
	stack->pop = stack_pop;
	stack->peek = stack_peek;
	stack->push = stack_push;
	return (stack);
}
