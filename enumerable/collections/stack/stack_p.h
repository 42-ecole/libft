/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_p.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/23 17:21:52 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/04 20:33:13 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_P_H
# define STACK_P_H

# include "stack.h"

void		stack_push(t_stack *s, void *data);
void		*stack_pop(t_stack *s);
void		*stack_peek(t_stack *s);
bool		stack_is_descorder(t_list *s);

#endif
