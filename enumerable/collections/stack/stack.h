/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/23 17:21:52 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/04 20:34:35 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_H
# define STACK_H

# include "../../sequences/list/list.h"
# include "stdbool.h"

typedef struct s_stack
{
	struct	s_list	*seq;
	void			(*push)(struct s_stack *, void *);
	void			*(*pop)(struct s_stack *);
	void			*(*peek)(struct s_stack *);
}				t_stack;

t_stack		*new_stack(size_t capacity, size_t content_size);

#endif
