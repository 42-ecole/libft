/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_manage.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/23 17:27:21 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/09 12:09:23 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"
#include "stack_p.h"
#include <stdio.h>

void		stack_push(t_stack *s, void *data)
{
	s->seq->add(s->seq, data);
}

void		*stack_pop(t_stack *s)
{
	void 	*obj;

	if (s->seq->count > 0)
	{
		obj = s->seq->index_of(s->seq, s->seq->count - 1);
		s->seq->remove(s->seq, -1); // -1 doesn't matter
		return (obj);
	}
	return (NULL);
}

void		*stack_peek(t_stack *s)
{
	if (!s->seq->count)
		return (NULL);
	return (s->seq->index_of(s->seq, s->seq->count - 1));
}