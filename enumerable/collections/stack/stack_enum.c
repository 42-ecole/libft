/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_enum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 10:50:21 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/09 10:51:06 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

bool	fstk_movenext(void *enumerable)
{
	t_stack *stack;

	stack = (t_stack *)enumerable;
	if (stack->ptr < &(stack->objs[stack->count - 1]))
	{
		stack->ptr = (stack->ptr + 1);
		return (true);
	}
	return (false);
}

void	fstk_reset(void *enumerable)
{
	t_stack *stack;

	stack = (t_stack *)enumerable;
	stack->ptr = stack->objs;
}

void	*fstk_current(void *enumerable)
{
	t_stack *stack;

	stack = (t_stack *)enumerable;
	return (stack->ptr);
}
