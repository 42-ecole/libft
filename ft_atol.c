/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Diana <Diana@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/09 18:30:46 by Diana             #+#    #+#             */
/*   Updated: 2020/10/09 18:31:20 by Diana            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_skip_blanks(const char *restrict str)
{
	size_t	i;

	i = ~0UL;
	while (str[++i]
	&& (((str[i]) >= 0x9 && (str[i]) <= 0xD) || (str[i]) == 0x20))
		;
	return (i);
}

int64_t	ft_atol(const char *str)
{
	int64_t	num;
	int64_t	sign;

	num = 0L;
	str += ft_skip_blanks(str);
	sign = ('-' == *str) ? -1L : 1L;
	if ('-' == *str || '+' == *str)
		++str;
	while (ft_isdigit(*str))
		num = num * 0xAL + *(str++) - 0x30L;
	return (num * sign);
}
